package com.example.logonrm.persistencia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class ArquitoActivity extends AppCompatActivity {


    private EditText edtFrase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arquito);

        edtFrase = (EditText) findViewById(R.id.edtFrase);

        //exibir dados do arquivo
        try {
            FileInputStream stream = openFileInput("frase.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            String linha = reader.readLine();
            Toast.makeText(this, linha, Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void gravar(View v){
        String frase = edtFrase.getText().toString();

        try {
            FileOutputStream stream = openFileOutput("frase.txt", MODE_PRIVATE);
            stream.write(frase.getBytes());
            stream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
