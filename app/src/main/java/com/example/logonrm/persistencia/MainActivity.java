package com.example.logonrm.persistencia;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private EditText edtUser;
    private EditText edtSenha;
    private CheckBox cbContinuarConectado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Recuperar preferencias
        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        boolean manterConectado = sp.getBoolean("manterConectado", false);
        String usuario = sp.getString("user", "Sem Usuário");


        /*if(manterConectado){
            Intent i = new Intent(this, home.class);
            startActivity(i);
        }*/




        edtUser  = (EditText) findViewById(R.id.edtUsuario);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        cbContinuarConectado = (CheckBox) findViewById(R.id.cbManterLogado);
    }

    public void gravar(View v){
        String usuario = edtUser.getText().toString();
        String senha = edtSenha.getText().toString();
        boolean manterConectado = cbContinuarConectado.isChecked();

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putBoolean("manterConectado", manterConectado);
        if(manterConectado) {
            editor.putString("user", usuario);
            editor.putString("senha", senha);
        }else{
            editor.putString("user", "");
            editor.putString("senha", "");
        }

        editor.commit();


        Intent i = new Intent(this, ArquitoActivity.class);
        startActivity(i);
    }
}
